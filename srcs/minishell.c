/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   minishell.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qdequele <qdequele@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/02 15:21:13 by qdequele          #+#    #+#             */
/*   Updated: 2016/03/03 13:19:51 by qdequele         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

static void	minishell_signals_exit(int i)
{
	(void)i;
	signal(SIGQUIT, SIG_DFL);
}

static void	minishell_signals(void)
{
	signal(SIGINT, minishell_signals_exit);
}

void		minishell_core(t_list **env, char **cmds)
{
	if (cmds[0] && builtins_find(cmds[0]))
		builtins_exec(env, cmds);
	else if (cmds[0])
		minishell_find_cmd(*env, cmds);
}

static void	minishell_get_cmds(void)
{
	char	*cmd;
	char	**cmds;
	char	**l_cmd;
	t_list	*env;
	int		i;

	print_shell();
	env = g_env;
	while (ft_get_next_line(0, &cmd))
	{
		i = 0;
		l_cmd = ft_strsplit(cmd, ';');
		while (l_cmd[i])
		{
			cmds = ft_str_to_tab(l_cmd[i]);
			minishell_core(&env, cmds);
			i++;
		}
		print_shell();
	}
}

int			main(int argc, char **argv, char **environ)
{
	(void)argv;
	(void)argc;
	g_env = NULL;
	env_parse_to_list(&g_env, environ);
	minishell_signals();
	minishell_get_cmds();
	return (0);
}
