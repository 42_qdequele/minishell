/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   minishell.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qdequele <qdequele@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/02 15:21:17 by qdequele          #+#    #+#             */
/*   Updated: 2016/03/03 13:32:52 by qdequele         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MINISHELL_H
# define MINISHELL_H
# include <libft.h>
# include <unistd.h>
# include <sys/stat.h>
# include <stdlib.h>
# include <fcntl.h>
# include <stdio.h>
# include <sys/types.h>
# include <errno.h>

typedef int	(*t_func)(t_list **env, char **cmds);

typedef struct stat	t_stat;

typedef struct		s_builtin
{
	char			*name;
	t_func			f;
}					t_builtin;

typedef struct		s_env
{
	char			*key;
	char			*value;
}					t_env;

t_list				*g_env;

/*
**	builtins
*/
int					builtins_cd(t_list **env, char **cmds);
int					builtins_env(t_list **env, char **cmds);
int					builtins_exit(t_list **env, char **cmds);
int					builtins_setenv(t_list **env, char **cmds);
int					builtins_unsetenv(t_list **env, char **cmds);
/*
**	builtins help
*/
int					builtins_find(char *cmd);
int					builtins_exec(t_list **env, char **cmds);
/*
**	env - env_parser.c
*/
void				env_parse_to_list(t_list **l_env, char **environ);
char				**env_parse_from_list(t_list *l_env);
/*
**	env - env_utils.c
*/
void				env_show(t_list *l_env);
char				*env_get(t_list *l_env, char *f_key);
void				env_add_or_modify(t_list **l_env, char *key, char *value);
void				env_remove(t_list *l_env, char *key);
/*
**	print - print_shell.c
*/
void				print_shell();
void				print_shell_err(char *s);
/*
**	minishell.c
*/
void				minishell_core(t_list **env, char **cmds);
void				minishell_find_cmd(t_list *env, char **cmds);
#endif
